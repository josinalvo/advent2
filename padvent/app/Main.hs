module Main where

import qualified Day15 as Day
import Data.Function.Memoize

main :: IO ()
main = Day.main

-- experimento de memoizacao
fib f 0 = 1
fib f 1 = 1
fib f n = f (n-1) + f (n-2)