fuel :: Integral a => a -> a
fuel n = if req > 0 then req else 0
    where req = (n `div` 3) - 2 

fuel2 n = sum $ tail $ takeWhile ( > 0) $ iterate fuel n

l :: [String]
l = ["12","14"]

main = do 
    s <- readFile "input1"
    print $ sum $ fmap fuel2 $ fmap read $ lines s