
import Control.Monad.Trans.State
import Data.List.Tools
import Data.List.Split


interpret 1 a b  = a+b
interpret 2 a b  = a*b


-- f_besta :: Int -> (Int,Int)
-- f_besta s = (s-1,s)

-- s_besta :: State Int Int
-- s_besta = state f_besta

-- a = runState s_besta 10

f :: (Int,[Int]) -> (Int, [Int])
f (pos,list)
    | (list !! pos == 99) = (pos, list)
    | otherwise           = (pos+4,setAt list np result)
    where opcode = list !! pos
          a  = list !! (list !! (pos + 1))
          b  = list !! (list !! (pos + 2))
          np = list !! (pos + 3)
          result = interpret opcode a b


solve list = head lastList
    where list_tuples = takeWhile' notFinished $ iterate f (0,list)
          (lastPos, lastList) = last list_tuples

takeWhile' prop [] = []
takeWhile' prop (a:as) = if not $ prop a
                         then [a]
                         else (a: takeWhile' prop as)

notFinished :: (Eq a, Num a) => (Int, [a]) -> Bool
notFinished (pos, list) = (opcode /= 99)
    where opcode = list !! pos

alterCode list = list2 
    where list1 = setAt list  1 12
          list2 = setAt list1 2 2

main = do 
    s <- readFile "input2"
    print $ solve $ alterCode $ fmap read $ splitOneOf [','] s