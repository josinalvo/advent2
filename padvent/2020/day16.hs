import Data.List.Tools
import Data.List.Split ( splitOn )
import Control.Monad ( join )
import Data.List
-- TODO stack overflow: https://hoogle.haskell.org/?hoogle=a%20-%3E%20%5Ba%5D%20-%3E%20%5B%5Ba%5D%5

s :: [String]
s = ["departure location: 1-3 or 5-6", "arrival: 2-5 or 7-9"]

myTicket :: [Integer]
myTicket = [103,197,83,101,109,181,61,157,199,137,97,179,151,89,211,59,139,149,53,107]

listOfPossibilities :: [String] -> [[String]]
listOfPossibilities []      = list_of_lists 1002
listOfPossibilities (s:strings) = mark2
    where rest = listOfPossibilities strings
          name:intervals:[] = splitOn ":" s
          interval1:interval2:[] = splitOn " or " intervals
          s1:f1:[] = fmap read $ splitOn "-" interval1
          s2:f2:[] = fmap read $ splitOn "-" interval2
          mark1 = mark rest  name s1 f1
          mark2 = mark mark1 name s2 f2

intersectMany :: Eq a => [[a]] -> [a]
intersectMany [] = undefined
intersectMany (l:[]) = l
intersectMany (l:ls) = intersect l $ intersectMany ls

invalid :: Eq a => [[a]] -> Int -> Bool
invalid listOfPossibilities number = listOfPossibilities !! number == []

allInvalids listOfPos = filter (invalid listOfPos) [0..1001]

validTicket invalidNumbers ticket = intersect invalidNumbers ticket == []

decypherPos pos tickets listOfPossibilities = names
    where vals = map (!! pos) tickets
          names = intersectMany $ map (listOfPossibilities !! ) vals

rangeOf list = take size infty
    where size  = length list
          infty = [0,1..]

stepOfExclusion lOfS = map rmSingles lOfS
    where singles = map head $ filter (\l -> length l == 1) lOfS
          rmSingles l = if length l > 1
                        then filter (\x -> not (x `elem` singles)) l
                        else l

exclusion l = converge stepOfExclusion l

converge f init = if init == new
                  then new
                  else converge f new
    where new = f init

main1 :: IO ()
main1 = do
    tickets <- readFile "input_nearby_day16"
    rules <- readFile  "input_rules_day16"
    let listOfPoss = listOfPossibilities $ lines rules
    let numbers = fmap read $ join $ fmap (splitOn ",") $ lines tickets
    print $ sum $ filter (invalid listOfPoss) numbers

main2 :: IO ()
main2 = do
    ticket_file <- readFile "input_nearby_day16"
    rules <- readFile  "input_rules_day16"
    let listOfPoss = listOfPossibilities $ lines rules
    let invalidNumbers = allInvalids listOfPoss
    let tickets = filter (validTicket invalidNumbers) $ fmap (fmap read) $ fmap (splitOn ",") $ lines ticket_file
    let pos     = rangeOf $ head tickets
    let which = fmap head $ exclusion $ fmap (\p -> decypherPos p tickets listOfPoss) $ pos
    let departure_indexes = filter (isPrefixOf "departure". (which !! )) $ rangeOf which
    let departure_vals    = map (myTicket !!) departure_indexes
    print $ product departure_vals
main = main2

list_of_lists 0    = []
list_of_lists size = []: list_of_lists (size-1)

mark lls name start finish = if start <= finish
                             then mark new_lls name (start+1) finish
                             else lls
    where new_lls = markp lls name start
          markp lls name position = setAt lls position new_list_possibilities
             where old_list = lls !! position
                   new_list_possibilities = if name `elem` old_list
                                            then old_list
                                            else name: old_list

-- |
-- >> list = list_of_lists 10
-- >> mark list "banana" 2 5
-- "hahaha"