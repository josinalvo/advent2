{-# LANGUAGE TupleSections #-}

module Day18 where
import Debug.Trace (trace)
import Data.Maybe


data StackItem = Open | TreeItem Tree | IncompleteNumberItem String | NumberItem Int deriving (Eq, Show)
data Tree = Null | Number {val :: Int} | Tree {left :: Tree, right :: Tree} deriving (Eq)

type Stack = [StackItem]

instance Show Tree where
    show (Tree l r) = "[" ++ show l ++ "," ++ show r ++ "]"
    show (Number a) = show a
    

showAsDraw = (`rShow` 0)

rShow :: Tree -> Int -> String
rShow (Number a)   offset = (whiteSpace offset) ++ show a ++ "\n"
rShow (Tree t1 t2) offset = (whiteSpace offset) ++ "Tree\n" ++ (rShow t1 (offset+1)) ++ (rShow t2 (offset+1))


whiteSpace:: Int -> String
whiteSpace a = take (4*a) (cycle "|   ")
    -- where justWhite = (take $ 4*(a-1)) (repeat ' ')

toTree :: StackItem -> Tree
toTree (TreeItem t)   = t
toTree (NumberItem a) = Number a

stackStep :: Char -> Stack -> Stack
-- stackStep a b | trace ("stackStep " ++ show a ++ show b ++ "\n\n") False = undefined
stackStep '[' stack = (Open:stack)
stackStep ',' ((IncompleteNumberItem top):stack) = NumberItem (read top): stack 
stackStep ',' stack = stack
stackStep ']' ((IncompleteNumberItem top):stack) = stackStep ']' (NumberItem (read top): stack) 
stackStep ']' (b:(a:(open:s))) = if open == Open 
                                 then newItem:s
                                 else error "not 2 it seems"
    where t1      = toTree a
          t2      = toTree b
          newItem = TreeItem $ Tree t1 t2
stackStep c ((IncompleteNumberItem top):stack) = IncompleteNumberItem (top++[c]):stack
stackStep c fullStack@(Open:stack)             = IncompleteNumberItem (c:[]):fullStack
stackStep c fullStack@((NumberItem n):stack)   = IncompleteNumberItem (c:[]):fullStack
stackStep c fullStack@((TreeItem t):stack)     = IncompleteNumberItem (c:[]):fullStack
stackStep _ _ = error "extra case??"



str2tree =  toTree . head . (foldl (flip stackStep) [])


example1      = "[[[[1,2],[3,4]],[[5,6],[7,8]]],9]"
example2      = "[[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]"

tree1 = str2tree example1
tree2 = str2tree example2

data BreadCrumb = LeftSibling Tree | RightSibling Tree deriving (Eq, Show)

type RestOfTree = [BreadCrumb]

type Zipper = (Tree,RestOfTree)

getTree :: Zipper -> Tree
getTree (tree,[]) = tree
getTree z = getTree . goUp $ z

zipTree :: Tree -> Zipper
zipTree = (,[])

goUp :: Zipper -> Zipper
goUp (tree,firstOfRest:rest) = (goodTree,rest)
    where fit (LeftSibling ts) t  = Tree ts t
          fit (RightSibling ts) t = Tree t ts
          goodTree                = fit firstOfRest tree
goUp (tree,[]) = (tree,[])
    
goLeft :: Zipper -> Zipper
goLeft (Tree l r,rest) = (l, RightSibling r:rest)
    
goRight :: Zipper -> Zipper
goRight (Tree l r,rest) = (r, LeftSibling l:rest)



addOne :: Zipper -> Zipper
addOne zip@(Tree _ _, _) = goUp rightReady
    where leftReady      = addOne (goLeft zip)
          rightReady     = addOne (goRight . goUp $ leftReady )
addOne (Number n, rest)  = (Number (n+1), rest)

tree1_added = (getTree . addOne . zipTree) tree1

leftMost :: Zipper -> Zipper
leftMost zip@(Tree _ _, _) = leftMost $ goLeft zip
leftMost zip@(Number _, _) = zip

rightMost :: Zipper -> Zipper
rightMost zip@(Tree _ _, _) = rightMost $ goRight zip
rightMost zip@(Number _, _) = zip

upMost :: Zipper -> Zipper
upMost (tree,[]) = (tree,[])
upMost z = upMost (goUp z)

nextLeaf :: Zipper -> Maybe Zipper
nextLeaf zip@(Number _ , RightSibling a: rest) = Just $ (leftMost . goRight . goUp) zip
nextLeaf zip@(Number _ , LeftSibling a : rest) = do 
    upperParent <- upUntilITookLeft zip
    return $ leftMost $ goRight upperParent

upUntilITookLeft :: Zipper -> Maybe Zipper
upUntilITookLeft zip@(_, RightSibling _: _) = Just (goUp zip)
upUntilITookLeft zip@(_, LeftSibling _: _) = (upUntilITookLeft . goUp) zip
upUntilITookLeft zip@(_, []) = Nothing

prevLeaf :: Zipper -> Maybe Zipper
prevLeaf zip@(Number _ , LeftSibling a  : rest) = Just $ (rightMost . goLeft . goUp) zip
prevLeaf zip@(Number _ , RightSibling a : rest) = do 
    upperParent <- upUntilITookRight zip
    return $ rightMost $ goLeft upperParent

upUntilITookRight :: Zipper -> Maybe Zipper
upUntilITookRight zip@(_, LeftSibling _: _) = Just (goUp zip)
upUntilITookRight zip@(_, RightSibling _: _) = (upUntilITookRight . goUp) zip
upUntilITookRight zip@(_, []) = Nothing


valOfNumber :: Zipper -> Int
valOfNumber (Number a, _) =  a

doExplode :: Zipper -> Zipper
doExplode (Tree (Number a)(Number b),crumbs) = secondAdd
    where zeroed = (Number 0, crumbs) :: Zipper
          leftZip :: Maybe Zipper
          leftZip = do
               (Number lNum, lCrumbs) <- prevLeaf zeroed
               nextLeaf (Number (lNum+a), lCrumbs)
          firstAdd :: Zipper
          firstAdd = if isNothing leftZip then zeroed else fromJust leftZip
          rightZip :: Maybe Zipper
          rightZip = do
              (Number rNum, rCrumbs) <- nextLeaf firstAdd
              prevLeaf (Number (rNum+b), rCrumbs)
          secondAdd :: Zipper
          secondAdd = if isNothing rightZip then firstAdd else fromJust rightZip
               
exploded1 = (getTree . doExplode . goRight . goLeft . goLeft) (tree1,[]) 

explodeOne :: Zipper -> Zipper
explodeOne zip@(Number a, c:crumbs) = fromMaybe zip next
    where isDeep = (>= 4) . length $ crumbs -- Potencial cagada
          checkCrumbForPair (RightSibling (Number _)) = True
          checkCrumbForPair _                         = False
          isPair = checkCrumbForPair c
          next   = if (isPair && isDeep) then Just (doExplode $ goUp zip) else explodeOne <$> nextLeaf zip
explodeOne zip = explodeOne start
    where start = (leftMost . upMost) zip

tree3 = str2tree "[[[[[9,8],1],10],3],4]"
exploded = (getTree . explodeOne) (tree3,[])

splitOne :: Zipper -> Zipper
splitOne zip@(Number a, crumbs) 
    | a >= 10 = (newPair, crumbs)
    | otherwise = fromMaybe zip (fmap splitOne $ nextLeaf zip)
    where newPair  = Tree (Number newSmall) (Number newBig)
          newSmall = a `div` 2  
          newBig   = a - newSmall
splitOne zip = splitOne start
    where start = (leftMost . upMost) zip

splitted = (getTree . splitOne) (tree3,[])
split2 = (getTree . splitOne . splitOne . zipTree . str2tree) "[[[[0,7],4],[15,[0,13]]],[1,1]]"
-- split3 = (getTree . splitOne . splitOne . explodeOne . explodeOne . zipTree . str2tree) "[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]"
split3 = (getTree . splitOne . explodeOne . explodeOne . zipTree . str2tree) "[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]"

converge f x = if (f x) == x
               then x
               else converge f (f x)

reduce :: Tree -> Tree
reduce t = getTree finalZip
    where inicialZip = zipTree t
          finalZip   = converge ( splitOne . upMost  . converge carefulExplode) inicialZip
          carefulExplode = upMost . explodeOne

simplesolve  = "[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]"
reduced      = reduce (str2tree simplesolve)

addReduce :: Tree -> Tree -> Tree
addReduce t1 t2 = reduce (Tree t1 t2)

simpleAdd :: Tree -> Tree -> Tree
simpleAdd = Tree

s1="[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]"
s2="[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]"

t1 = str2tree s1
t2 = str2tree s2
z21 = zipTree $ simpleAdd t1 t2

sumAndSolve :: [Tree] -> Tree
sumAndSolve trees = foldl1 addReduce trees
    
          

-- |
-- >>> small_part_05 = sumAndSolve <$> (fmap str2tree) <$> lines <$> readFile "2021/input18small"
-- >>> small_part_05 
-- [[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]
-- >>> magnitude <$> small_part_05
-- 3488

-- |
-- >>> part_10 =  (fmap str2tree) <$> lines <$> readFile "2021/input18"
-- >>> magnitude <$> sumAndSolve <$> part_10
-- 4145
-- >>> largestMagnitude <$> part_10
-- 4855

magnitude :: Tree -> Int
magnitude (Tree a b) = (3 * magnitude a) + (2 * magnitude b)
magnitude (Number n) = n


largestMagnitude :: [Tree] -> Int
largestMagnitude trees = maximum magnitudes
    where pairs = [(a,b) | a <- trees, b <- trees, a /= b ]
          magnitudes = map (magnitude . uncurry addReduce) pairs

-- |
-- >>> valOfNumber $ goRight (tree1,[])
-- 9
-- >>> valOfNumber $ leftMost (tree1,[])
-- 1
-- >>> n1 = (fromJust . nextLeaf)
-- >>> valOfNumber $ n1 $ leftMost (tree1,[])
-- 2
-- >>> n2 = (n1 . n1)
-- >>> valOfNumber $ n2 $ leftMost (tree1,[])
-- 3
-- >>> n3 = (n1 . n2)
-- >>> valOfNumber $ n3 $ leftMost (tree1,[])
-- 4
-- >>> n4 = (n1 . n3)
-- >>> valOfNumber $ n4 $ leftMost (tree1,[])
-- 5
-- >>> n5 = (n1 . n4)
-- >>> valOfNumber $ n5 $ leftMost (tree1,[])
-- 6
-- >>> n6 = (n1 . n5)
-- >>> valOfNumber $ n6 $ leftMost (tree1,[])
-- 7
-- >>> n7 = (n1 . n6)
-- >>> valOfNumber $ n7 $ leftMost (tree1,[])
-- 8
-- >>> n8 = (n1 . n7)
-- >>> valOfNumber $ n8 $ leftMost (tree1,[])
-- 9
-- >>> n9 = (nextLeaf . n8)
-- >>> n9 $ leftMost (tree1,[])
-- Nothing


-- |
-- >>> valOfNumber $ rightMost (tree1,[])
-- 9
-- >>> n1 = (fromJust . prevLeaf)
-- >>> valOfNumber $ n1 $ rightMost (tree1,[])
-- 8
-- >>> n2 = (n1 . n1)
-- >>> valOfNumber $ n2 $ rightMost (tree1,[])
-- 7
-- >>> n3 = (n1 . n2)
-- >>> valOfNumber $ n3 $ rightMost (tree1,[])
-- 6
-- >>> n4 = (n1 . n3)
-- >>> valOfNumber $ n4 $ rightMost (tree1,[])
-- 5
-- >>> n5 = (n1 . n4)
-- >>> valOfNumber $ n5 $ rightMost (tree1,[])
-- 4
-- >>> n6 = (n1 . n5)
-- >>> valOfNumber $ n6 $ rightMost (tree1,[])
-- 3
-- >>> n7 = (n1 . n6)
-- >>> valOfNumber $ n7 $ rightMost (tree1,[])
-- 2
-- >>> n8 = (n1 . n7)
-- >>> valOfNumber $ n8 $ rightMost (tree1,[])
-- 1
-- >>> n9 = (prevLeaf . n8)
-- >>> n9 $ rightMost (tree1,[])
-- Nothing

-- |
-- >>> exploded1
-- [[[[1,5],0],[[9,6],[7,8]]],9]



















-- import Data.Sequence
-- import Debug.Trace (trace)
-- type Val   = Int 
-- type Depth = Int

-- example1      = "[[[[1,2],[3,4]],[[5,6],[7,8]]],9]"
-- example2      = "[[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]"

    
-- readWithDepth :: String -> Int -> String -> [(Val,Depth)]
-- --readWithDepth a b c | trace ("read " ++ a ++ show b ++ "accumulator" ++ c ++ "\n\n") False = undefined

-- readWithDepth ('[':string) currentDepth accString = readWithDepth string (currentDepth + 1) accString
-- readWithDepth (']':string) currentDepth accString 
--     | accString == "" = readWithDepth string (currentDepth - 1) accString
--     | otherwise       = (,) (read accString) currentDepth : readWithDepth string (currentDepth - 1) emptyAccString
--     where emptyAccString = ""
-- readWithDepth (',':string) currentDepth accString = if accString == "" then rest else first : rest
--     where emptyAccString = ""
--           rest = readWithDepth string currentDepth emptyAccString
--           first = (,) (read accString) currentDepth
-- readWithDepth (c:string) currentDepth accString = readWithDepth string currentDepth (accString ++ [c])
-- readWithDepth "" _ _                            = []

-- -- |
-- -- >>> readWithDepth example1 0 ""
-- -- [(1,4),(2,4),(3,4),(4,4),(5,4),(6,4),(7,4),(8,4),(9,1)]

-- -- |
-- -- >>> readWithDepth example2 0 ""
-- -- [(9,3),(3,4),(8,4),(0,4),(9,4),(6,3),(3,4),(7,4),(4,4),(9,4),(3,2)]



