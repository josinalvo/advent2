module Day15 where

import Data.Vector (Vector, fromList, (!?))
import Data.Maybe ( isJust, fromJust, isNothing )

import Debug.Trace (trace)
import qualified Data.Set as Set
import Data.Set (empty, lookupMin, deleteMin, Set)
import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map)
import Control.Monad (join)


type World     = Vector (Vector Int)
type Position  = (Int,Int)
type Cost      = Int
type Distances = Map Position Int
type Pending   = Set (Cost,Position)

smallExample = "1163751742\n1381373672\n2136511328\n3694931569\n7463417111\n1319128137\n1359912421\n3125421639\n1293138521\n2311944581"
-- 1163751742
-- 1381373672
-- 2136511328
-- 3694931569
-- 7463417111
-- 1319128137
-- 1359912421
-- 3125421639
-- 1293138521
-- 2311944581

str2world :: String -> World
str2world string = fromList vLines
    where charLines   = lines string :: [String]
          intLines = fmap (fmap char2int) charLines :: [[Int]]
          vLines :: [Vector Int]
          vLines   = fmap fromList intLines
          char2str = (:[])

char2int '0' = 0
char2int '1' = 1
char2int '2' = 2
char2int '3' = 3
char2int '4' = 4
char2int '5' = 5
char2int '6' = 6
char2int '7' = 7
char2int '8' = 8
char2int '9' = 9

(+>) :: Position -> Position -> Position
(+>) (x1,y1) (x2,y2) = (x1+x2,y1+y2)

readVal :: World -> Position -> Maybe Int 
readVal world (x,y)= do
    line <- world !? x
    line !? y

-- |
-- >>> world = str2world smallExample
-- >>> readVal world (0,0)
-- Just 1
-- >>> readVal world (0,8)
-- Just 4
-- >>> readVal world (0,9)
-- Just 2
-- >>> readVal world (0,10)
-- Nothing
-- >>> readVal world (0,10)
-- Nothing
-- >>> readVal world (4,6)
-- Just 7
-- >>> readVal world (9,9)
-- Just 1
-- >>> readVal world (9,8)
-- Just 8
-- >>> readVal world (10,8)
-- Nothing

neighboors :: World -> Position -> [Position]
neighboors world pos = insiders
    where candidates = pure (+>) <*> [(1,0),(0,1),(-1,0),(0,-1)] <*> [pos]
          insiders   = [c | c <- candidates, isJust $ readVal world c]

-- |
-- >>> world = str2world smallExample
-- >>> neighboors world (0,0)
-- [(1,0),(0,1)]
-- >>> neighboors world (9,4)
-- [(9,5),(8,4),(9,3)]

end :: World -> Position -> Bool
end world pos = noZero && fewNeis
    where noZero  = (fst pos * snd pos) /= 0
          fewNeis = length (neighboors world pos) == 2

-- |
-- >>> world = str2world smallExample
-- >>> end world (8,9)
-- False
-- >>> end world (0,0)
-- False
-- >>> end world (9,9)
-- True

a = empty
b = Set.insert 12 a
c = Set.insert 15 b
d = Set.insert 11 c
(eleven,c') = (,) (lookupMin d) (deleteMin d)

-- |
-- >>> eleven
-- Just 11
-- >>> c'
-- fromList [12,15]
-- >>> (twelve,b') = (,) (lookupMin c') (deleteMin c')
-- >>> twelve
-- Just 12
-- >>> b'
-- fromList [15]
-- >>> (fifthteen,a') = (,) (lookupMin b') (deleteMin b')
-- >>> fifthteen
-- Just 15
-- >>> a'
-- fromList []
-- >>> (nope,vazio) = (,) (lookupMin a') (deleteMin a')
-- >>> nope
-- Nothing
-- >>> vazio
-- fromList []

smallestInsertable ::  Distances -> Pending -> (Maybe (Position, Cost) ,Pending)
smallestInsertable  ds pen 
    | null pen           = (Nothing, Set.empty )
    | otherwise          = if    position `Map.notMember` ds
                           then  (Just (position, cost),Set.deleteMin pen)
                           else  smallestInsertable ds (Set.deleteMin pen)
    where (cost,position) = fromJust $ lookupMin pen

addOne :: World -> (Distances,Pending) -> (Distances,Pending)
addOne world (distances,pending) = if isNothing toInsert
                                   then (distances, reducedPending)
                                   else (newDistances, newPending) 
    where (toInsert, reducedPending) = smallestInsertable distances pending
          (position,cost)            = fromJust toInsert
          newDistances               = Map.insert position cost distances
          neigs                      = neighboors world position
          costs                      = map ((+ cost) . fromJust . readVal world) neigs
          newPending                 = Set.union (Set.fromList $ zip costs neigs) reducedPending

converge :: Eq t => (t -> t) -> t -> t
converge f a = if a == f a
               then a
               else converge f (f a)

calculateDistances world = converge (addOne world) (Map.empty,Set.fromList [(0,(0,0))]) 


-- calculateDistancesAux world (distances, pending) = if null pending
--                                                    then (distances,pending)
--                                                    else calculateDistancesAux world (newDistances, newPending)
--                     where (newDistances, newPending) = addOne world (distances,pending)

-- calculateDistances world = calculateDistancesAux world (Map.empty,Set.fromList [(0,(0,0))])

verySmallExample = "12\n34\n"

-- |
-- >>> (Map.!? (9,9)) . fst $ calculateDistances $ str2world smallExample
-- Just 40

solve1 :: IO (Maybe Int)
solve1 = do
    input <- readFile "2021/input15"
    let world = str2world input
    -- print $ show (99,99)
    -- print $ end world (99,99)
    return $ ((Map.!? (99,99)) . fst) (calculateDistances world)

main1 :: IO ()
main1 = solve1 >>= print

-- |
-- >>> solve1
-- Just 366


increaseChars :: String -> String 
increaseChars = fmap wrapAround
    where wrapAround '1' = '2'
          wrapAround '2' = '3'
          wrapAround '3' = '4'
          wrapAround '4' = '5'
          wrapAround '5' = '6'
          wrapAround '6' = '7'
          wrapAround '7' = '8'
          wrapAround '8' = '9'
          wrapAround '9' = '1'

-- |
-- >>> increaseChars "123456789"
-- "234567891"

increaseLine :: String -> String 
increaseLine s = s ++ i s ++ (i.i) s ++ (i.i.i) s ++ (i.i.i.i) s 
    where i = increaseChars

extraLines :: [String] -> [String]
extraLines sL = sL ++ i sL ++ (i.i) sL ++ (i.i.i) sL ++ (i.i.i.i) sL
    where i = fmap increaseChars

makeBig :: String -> String
makeBig s = join $ fmap (++ "\n") $ (extraLines . fmap increaseLine) $ lines s

largeExample :: String
largeExample = makeBig smallExample


-- |
-- >>> (Map.!? (49,49)) . fst $ calculateDistances $ str2world largeExample
-- Just 315

memoizedFib :: Int -> Integer
-- memoized_fib pos | trace (" pos: " ++ show pos ++ "\n") False = undefined
memoizedFib = (map fib [0 ..] !!)
   where fib 0 = 0
         fib 1 = 1
         fib n = memoizedFib (n-2) + memoizedFib (n-1)


main2 = do
    input <- readFile "2021/input15"
    let world = str2world $ makeBig input
    -- print $ show (99,99)
    -- print $ end world (99,99)
    print $ ((Map.!? (499,499)) . fst) (calculateDistances world)

-- |
-- >>> main2
-- Just 2829
main = main2





-- nonsensical bulshit I spent some time with
-- smallest :: World -> Visited -> Position  -> Int
-- smallest _ visited pos | trace ("visited " ++ show visited ++ " pos: " ++ show pos ++ "\n") False = undefined
-- smallest world visited pos  = if   end world pos
--                               then fromJust (readVal world pos)
--                               else fromJust (readVal world pos) + minimum options
--     where neighs     = filter (`notMember` visited) $ neighboors world pos
--           newVisited = foldl (flip insert) visited neighs
--           options    = [12121212] ++ fmap (smallest world newVisited) neighs

-- positions world = do
--     x <- [0..400]
--     y <- [0..400]
--     [(x,y)| isJust $ readVal world (x,y)]

-- smallestMemo :: World -> Visited -> Position  -> Int
-- smallestMemo world visited pos = fromJust $ memoMap Map.!? pos
--     where costs             = fmap interSmallest (positions world)
--           tuples            = zip (positions world) costs
--           memoMap           = Map.fromList tuples
--           interSmallest pos | trace (" pos: " ++ show pos ++ "\n") False = undefined
--           interSmallest pos = if   end world pos
--                               then fromJust (readVal world pos)
--                               else fromJust (readVal world pos) + minimum options
--             where neighs     = filter (`notMember` visited) $ neighboors world pos
--                   newVisited = foldl (flip insert) visited neighs
--                   options    = [12121212] ++ fmap (smallestMemo world newVisited) neighs

-- -- smallestMemo :: World -> Visited -> Position  -> Int

-- fibMemo = (map fib [0..] !!)
--     where fib 0 = 1
--           fib 1 = 1
--           fib n = fibMemo (n-1) + fibMemo (n-2)
