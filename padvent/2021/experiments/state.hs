import Control.Monad.Trans.State ( state )

data TurnstileState = Locked | Unlocked
  deriving (Eq, Show)

data TurnstileOutput = Thank | Open | Tut
  deriving (Eq, Show)

coin, push :: TurnstileState -> (TurnstileOutput, TurnstileState)

coin _ = (Thank, Unlocked)

push Locked   = (Tut , Locked)
push Unlocked = (Open, Locked)

--outputs
-- estados
--acoes (funcoes)

-- pushS :: StateT TurnstileState ghc-prim-0.6.1:GHC.Types.Any TurnstileOutput
-- pushS = state push

main = print 21
