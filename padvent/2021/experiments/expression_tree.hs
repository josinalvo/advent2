-- this module is meant to only process fully parenthesised trees

data StackItem = Open | Close | StackTree Tree
data Tree = Number Int | Tree Tree Tree

str2tree :: String -> Tree