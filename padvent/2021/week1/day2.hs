
import Data.List.Split ( splitOn )
import Data.List (foldl')
data Position = Position {depth :: Int, horizontal :: Int, aim :: Int} deriving Show 


process2 :: [(String, Int)] -> Position
process2 [] = Position {depth = 0, horizontal = 0, aim = 0}
process2 ((movementName,amount):comms) 
    | movementName == "forward" = rest {horizontal = r_hor+amount, depth = r_dep + r_aim*amount}
    | movementName == "down"    = rest {aim = r_aim+amount}
    | movementName == "up"      = rest {aim = r_aim-amount}


    where rest  = process2 comms
          r_hor = horizontal rest
          r_dep = depth rest
          r_aim = aim rest
process2 _ = undefined 

process :: [(String, Int)] -> Position
process commands = foldl' f zero commands 
    where zero = Position 0 0 0
          f pos@Position {depth=r_dep, horizontal=r_hor, aim=r_aim} (movementName,amount)  
           | movementName == "forward" = pos {horizontal = r_hor+amount}
           | movementName == "down"    = pos {depth = r_dep+amount}
           | movementName == "up"      = pos {depth = r_dep-amount}
          f _ _ = undefined

str2command :: String -> (String,Int)
str2command string = (movementNameS,(read amountS))
    where (movementNameS:amountS:[]) = splitOn " " string

main1 = do
    s <- readFile "input2"
    print $ process $ fmap str2command (lines s)

main2 = do
    s <- readFile "input2"
    print $ process2 $ fmap str2command $ reverse (lines s)