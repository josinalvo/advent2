

import Control.Applicative
import Debug.Trace

import Data.Vector (Vector)

-- |
-- >>> 1+1
-- 2

str2zip :: String -> ZipList Int
str2zip str = ZipList $ map (read . (:[])) str 

sums :: [ZipList Int] -> ZipList Int
sums listZips = foldr (liftA2 (+)) (pure 0) listZips

high :: (Int,ZipList Int) -> ZipList Int
high (size,sums) = fmap takeHigh sums
    where takeHigh :: Int -> Int
          takeHigh d = if fromIntegral d >= (fromIntegral size/2) then 1 else 0
          

low :: (Int,ZipList Int) -> ZipList Int
low (size,sums) = fmap takeLow sums
    where takeLow d 
            | d*2 <  size  = 1 -- ones are less then half
            | d*2 == size  = 0 -- ones are half
            | d*2 >  size  = 0 -- ones are more/zeros are less then half
            | otherwise    = error "this should never run"

          
fromBinary :: ZipList Int -> Int
fromBinary list = foldl (\x y -> x*2+y) 0 list

type Pos = Int


-- TODO nao é possivel q nao haja funcao pronta
(!!!) :: ZipList a -> Int -> a
(!!!) l a = getZipList l !! a

digits f oldList = f $ (,) (length oldList) (sums oldList)

filterOn :: ((Int,ZipList Int) -> ZipList Int) -> ([ZipList Int],Pos) -> ([ZipList Int],Pos)
--filterOn a b | trace ("filterOn " ++ show b ++ "\n") False = undefined
filterOn f (oldList, pos) = if length oldList == 1
                            then (oldList, pos)
                            else (newList, newPos)
    where calculatedDigits = digits f oldList
          sameInPos :: Int -> (ZipList Int) -> (ZipList Int) -> Bool
          sameInPos p l1 l2 = (l1 !!! p) == (l2 !!! p)
          newList :: [ZipList Int]
          newList = filter (sameInPos pos calculatedDigits) oldList
          newPos  = if newList == oldList then  pos else (pos + 1) `rem` size
          size    = length $ head oldList

converge :: Eq t => (t -> t) -> t -> t
converge f init = if init == new
                  then new
                  else converge f new
    where new = f init

-- TOMAYBE: tirar esse fromBinary
main1 = do
    s <- readFile "input3"
    let zips = map str2zip $ lines s
    let resp = ((*) <$> (fromBinary . high) <*> (fromBinary . low)) $ ((,) <$> length <*> sums) zips
    print resp

main2 = do
    s <- readFile "input3"
    let zips = fmap str2zip $ lines s
    let (ox,p1)  = converge (filterOn high) (zips,0)
    let (nox,p2) = converge (filterOn low) (zips,0)
    print $ (,) (fromBinary $ Prelude.head ox)  (fromBinary $ Prelude.head nox)
    print $ (*) (fromBinary $ Prelude.head ox)  (fromBinary $ Prelude.head nox)

main = main2