import Data.List.Split
import Data.List.Tools
import Data.List (elemIndex)
import Data.Maybe

readCard :: [String] -> [Int]
readCard ("":lStr)  = readCard lStr
readCard (str:lStr) = numbers ++ rest
    where numbers = fmap read $ filter (/= "") $ splitOn " " str
          rest    = readCard lStr
readCard []       = []

numberAt :: [Int] -> Int -> Maybe (Int, Int)
numberAt card number = do
          pos <- elemIndex number card
          let column = pos `rem` 5
          let line   = pos `div` 5
          return (line,column)

example2 :: [String]
example2 = ["1 22 3 14 59 ", 
        " 20 21 22 23 24 ",
        " 30 31 32 33 34 ",
        " 40 41 42 43 100",
        " 50 61 52 53 54 "]

example_win =   ["14 21 17 24  4",
                 "10 16 15  9 19",
                 "18  8 23 26 20",
                 "22 11 13  6  5",
                 " 2  0 12  3  7"]

draws_win :: [Int]
draws_win = [7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1]

-- |
-- >>> numberAt (readCard example2 ) 22
-- Just (0,1)
-- >>> numberAt (readCard example2) 33
-- Just (2,3)
-- >>> numberAt (readCard example2) 100
-- Just (3,4)
-- >>> numberAt (readCard example2) 50
-- Just (4,0)
-- >>> numberAt (readCard example2) 56
-- Nothing

type RowResults = [Int]
type LineResults = [Int]
type Results = (LineResults,RowResults)

considerNum :: [Int] -> Results -> Int -> Results
considerNum card (lres, rres) newDraw = if isNothing pos
                                        then (lres, rres) 
                                        else (newLRes, newRRes)
    where pos = numberAt card newDraw
          Just (addLine,addRow) = pos
          newLRes = setAt lres addLine (lres !! addLine + 1)
          newRRes = setAt rres addRow  (rres !! addRow + 1)


isWin :: Results -> Bool
isWin (r1,r2) = 5 `elem` r1++r2

-- foldl'' f start (x:xs) = result1:(foldl'' f result1 xs)
--     where result1 = f start x

victoryIn :: [Int] -> [Int] -> Maybe Int
victoryIn card draws = positionVictor
    where resultsTillWin :: [Results]
          resultsTillWin = drop 1 $ takeWhile (not . isWin) $ scanl (considerNum card) start draws
          zeroes = [0,0,0,0,0]
          start  = (zeroes,zeroes) :: Results
          positionVictor = if length resultsTillWin < length draws
                           then Just (length resultsTillWin)
                           else Nothing 

points card draws = do
    victoryPos <- victoryIn card draws
    let lastDraw    = draws !! victoryPos
    let usefulDraws = lastDraw : take victoryPos draws
    let unusedNums  = filter (not . (`elem` usefulDraws)) card
    Just $ sum unusedNums * lastDraw
    -- Just $ (usefulDraws,lastDraw)

-- |
-- >>> points (readCard example_win) draws_win
-- Just 4512 

readDraws :: String -> [Int]
readDraws s = (fmap read) $ splitOn "," s

-- TODO lembrar de newtype versus type versus data    

smallest :: [Maybe Int] -> Maybe Int
smallest mNums = minimum jNumns
    where jNumns = filter isJust mNums

biggest :: [Maybe Int] -> Maybe Int
biggest mNums = maximum jNumns
    where jNumns = filter isJust mNums

justify (Just a) = a
main = do
    s <- lines <$> readFile "input4"
    let draws         = readDraws $ head s
    let cards         = map readCard $ splitOn [""] $ tail s
    let results       = fmap (`victoryIn` draws) cards :: [Maybe Int]
    --let choice        = smallest results
    let choice        = biggest results
    let (Just pos)    = elemIndex choice results
    let winningCard   = cards !! pos
    let winningPoints = points winningCard draws
    let a  = 1
    print (cards !! a, results !! a, take (justify $ results !! a) draws)
    print (filter ((/= 25) . length) cards )
    print winningPoints
    

