
increasing :: [Int] -> Int

increasing [] = undefined
increasing (x1:[]) = 0

increasing (x1:x2:xs) = this_step + rest
    where this_step = if (x1 < x2) then 1 else 0
          rest      = increasing (x2:xs) 

windows :: Num a => [a] -> [a]
windows (x1:x2:x3:xs) = (x1+x2+x3):(windows (x2:x3:xs))
windows (x1:x2:[]) = []
windows _ = error "Nooo"

main1 = do 
    s <- readFile "input1"
    -- print $ fmap increasing $ fmap read $ lines s
    let r = (read <$> lines s) :: [Int]
    print $ increasing r

main2 = do 
    s <- readFile "input1"
    -- print $ fmap increasing $ fmap read $ lines s
    let r = (read <$> lines s) :: [Int]
    print $ increasing $ windows r

