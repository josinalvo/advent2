{-# LANGUAGE TupleSections #-}
import Data.List.Split
import Data.Map.Strict as Map

import Data.List

bigInput :: [Integer]
bigInput = [1,1,1,2,1,1,2,1,1,1,5,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,1,4,1,1,1,1,3,1,1,3,1,1,1,4,1,5,1,3,1,1,1,1,1,5,1,1,1,1,1,5,5,2,5,1,1,2,1,1,1,1,3,4,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,5,4,1,1,1,1,1,5,1,2,4,1,1,1,1,1,3,3,2,1,1,4,1,1,5,5,1,1,1,1,1,2,5,1,4,1,1,1,1,1,1,2,1,1,5,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4,3,1,1,3,1,3,1,4,1,5,4,1,1,2,1,1,5,1,1,1,1,1,5,1,1,1,1,1,1,1,1,1,4,1,1,4,1,1,1,1,1,1,1,5,4,1,2,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,4,1,1,1,2,1,4,1,1,1,1,1,1,1,1,1,4,2,1,2,1,1,4,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,3,2,1,4,1,5,1,1,1,4,5,1,1,1,1,1,1,5,1,1,5,1,2,1,1,2,4,1,1,2,1,5,5,3]

smallInput :: [Integer]
smallInput = [3,4,3,1,2]

step 0 = [6,8]
step n = [n-1]

fishesToCounts :: [Integer] -> Map Integer Integer
fishesToCounts list = simpleMap
    where simpleMap = fromListWith (+) $ fmap (,1) list

stepAll :: Map Integer Integer -> Map Integer Integer
stepAll fishesMap = fromList pairsSum
    where pairsRepeat :: [(Integer,Integer)]
          pairsRepeat = do
                        (k,count) <- toList fishesMap
                        step <- step k
                        [(step,count)] 
          pairsSum :: [(Integer,Integer)]
          pairsSum    = do
                        k <- nub $ fmap fst pairsRepeat
                        let amountK = sum $ Prelude.map snd $ Prelude.filter ( (k ==). fst ) pairsRepeat
                        [(k,amountK)]

allSteps input amount = Prelude.foldl (\ map _ -> stepAll map) startMap [1..amount]
    where startMap = fishesToCounts input

count map = sum $ elems map

main = do 
    print $ count $ allSteps bigInput 256 --part1 is 80


