
{-# LANGUAGE TupleSections #-}
import Data.List.Split
import Data.Map.Strict as Map
import Control.Monad




verS = "777,778 -> 777,676"
horS = "500,510 -> 378,510"
generalS = "702,85 -> 44,90"

hor :: Line
ver :: Line
general :: Line
[ver,hor,general] = fmap str2line [verS, horS, generalS]

data Line = Line {xFirst :: Int, yFirst :: Int, xSecnd :: Int, ySecnd :: Int}


str2line string = Line {xFirst = nums !! 0,yFirst = nums !! 1,xSecnd = nums !! 2,ySecnd = nums !! 3}
    where first = splitOn "->" string !! 0
          secnd = splitOn "->" string !! 1
          numsS = splitOn "," first ++ splitOn "," secnd
          nums  = fmap read numsS

isHor :: Line -> Bool
isHor  = uncurry (==) . fpair
   where fpair = (,) <$> yFirst <*> ySecnd

isVer :: Line -> Bool
isVer  = uncurry (==) . fpair
   where fpair = (,) <$> xFirst <*> xSecnd

isDia :: Line -> Bool 
isDia  = uncurry (==) . fpair
   where fpair = (,) <$> distX <*> distY
         distX :: Line -> Int
         distX = abs . ((-) <$> xFirst <*> xSecnd)
         distY :: Line -> Int
         distY = abs . ((-) <$> yFirst <*> ySecnd)

-- |
-- >>> (isHor hor, isHor ver, isHor general)
-- (True,False,False)
-- >>> (isVer hor, isVer ver, isVer general)
-- (False,True,False)

-- toPairs line

interval :: Int -> Int -> [Int]
interval a b = [start .. finish]
    where start :: Int
          start      = min a b
          finish :: Int
          finish     = max a b

pointsHor :: Line -> [(Int,Int)]
pointsHor l = Prelude.map commonPart changingPart
    where commonPart :: Int -> (Int,Int)
          commonPart = (,yFirst l) 
          changingPart = interval (xFirst l) (xSecnd l)
         

pointsVer :: Line -> [(Int,Int)]
pointsVer l = Prelude.map commonPart changingPart
    where commonPart :: Int -> (Int,Int)
          commonPart = (xFirst l,)
          changingPart = interval (yFirst l) (ySecnd l) 

orderedInterval :: Int -> Int -> [Int]
orderedInterval a b = if a <= b
                      then interval a b
                      else reverse $ interval b a

pointsDia :: Line -> [(Int,Int)]
pointsDia l = zip xs ys
    where xs = orderedInterval (xFirst l) (xSecnd l)
          ys = orderedInterval (yFirst l) (ySecnd l)

line2points :: Line -> [(Int,Int)]
line2points line 
    | isHor line = pointsHor line
    | isVer line = pointsVer line
    | isDia line = pointsDia line
    | otherwise  = []

-- count :: (Eq a, Ord a) => [a] -> Map a Int
-- count list = Prelude.foldl newMap Map.empty list
--     where newMap :: (Eq a, Ord a) => Map a Int -> a -> Map a Int
--           newMap map a = if a `elem` (keys map)
--                          then adjust (+1) a map
-- --                          else insert a 1 map
-- repeated :: Eq a => Map.Map a Int -> [a]
-- repeated maaa = Map.keys $ Map.filter (>= 2) maaa

repeated :: [(Int,Int)] -> Int 
repeated list = length $ Map.keys $ Map.filter (>= 2) simpleMap 
    where simpleMap = fromListWith (+) $ fmap (,1) list

test :: Map.Map String Int
test = Map.fromList [("c",1),("d",5),("b",2)]





main = do
    line_list <- fmap str2line . lines <$> readFile "input5"
    let pairs = join $ fmap line2points line_list
    print $ repeated pairs
    