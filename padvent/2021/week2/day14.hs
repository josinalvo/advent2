
{-# LANGUAGE TupleSections #-}

import Data.Map.Strict
import Data.List.Extra (splitOn)
import Data.Maybe (fromJust)
import Control.Arrow (ArrowChoice(left))

type Rules = Map Pair Char
type Rule = (Pair,Char)
type CountPair = Map Pair Int
type Pair = (Char,Char)


toRule :: String -> Rule
toRule   string   = ((a,b),c)
    where [[a,b],(c:_)] = splitOn " -> " string

getInput :: String -> (String,Rules)
getInput string = (starting,fromList ruleList)
    where starting    = head $ lines string 
          ruleStrings = (tail . tail . lines) string
          ruleList    = fmap toRule ruleStrings

testRules :: [Char]
testRules = "NNCB\n\nCH -> B\nHH -> N\nCB -> H\nNH -> C\nHB -> C\nHC -> B\nHN -> C\nNN -> C\nBH -> H\nNC -> B\nNB -> B\nBN -> B\nBB -> N\nBC -> B\nCC -> N\nCN -> C"

-- |
-- >>> getInput testRules
-- ("NNCB",fromList [(('B','B'),'N')...,(('N','N'),'C')])

countString :: String -> CountPair
countString (a:[])     = fromList []
countString []         = fromList []
countString (a:b:rest) = insertWith (+) newPair 1 countRest
    where countRest :: CountPair
          countRest = countString (b:rest)
          newPair   = (a,b)

test1 = countString start
    where (start,rules) = getInput testRules

-- |
-- >>> test1
-- fromList [(('C','B'),1),(('N','C'),1),(('N','N'),1)]

expandPairOnce ::  Rules ->  Int -> Pair -> CountPair 
expandPairOnce rules amount pair =   if first == second 
                                     then fromList [(first,2*amount)]
                                     else fromList [(first,amount),(second,amount)]
    where newChar = fromJust (rules !? pair)
          first   = (,) (fst pair) newChar
          second  = (,) newChar (snd pair) 

expandPairsOnce :: Rules -> CountPair -> CountPair
expandPairsOnce r old_counts = unionsWith (+) expanded
    where l_old_counts = toList old_counts
          expanded     = fmap (\(pair,amount) -> expandPairOnce r amount pair) l_old_counts

test2 = expand (countString start)
    where (start,rules) = getInput testRules
          expand        = expandPairsOnce rules

test3 = (expand . expand) (countString start)
    where (start,rules) = getInput testRules
          expand        = expandPairsOnce rules
test4 = (expand . expand . expand) (countString start)
    where (start,rules) = getInput testRules
          expand        = expandPairsOnce rules

test5 = (expand . expand . expand . expand) (countString start)
    where (start,rules) = getInput testRules
          expand        = expandPairsOnce rules

-- |
-- >>> test2 == countString "NCNBCHB"
-- True
-- >>> test3 == countString "NBCCNBBBCBHCB"
-- True
-- >>> test4 == countString "NBBBCNCCNBBNBNBBCHBHHBCHB"
-- True
-- >>> test5 == countString "NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB"
-- True



expandPairsManyTimes :: Rules -> CountPair -> Int -> CountPair
expandPairsManyTimes r old_counts 0 = old_counts
expandPairsManyTimes r old_counts n = expandPairsOnce r smaller
    where smaller = expandPairsManyTimes r old_counts (n-1)

testPrime n = expandN (countString start) n
    where (start,rules) = getInput testRules
          expandN       = expandPairsManyTimes rules 

test2' = testPrime 1
test3' = testPrime 2
test4' = testPrime 3
test5' = testPrime 4

-- |
-- >>> test2' == countString "NCNBCHB"
-- True
-- >>> test3' == countString "NBCCNBBBCBHCB"
-- True
-- >>> test4' == countString "NBBBCNCCNBBNBNBBCHBHHBCHB"
-- True
-- >>> test5' == countString "NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB"
-- True
-- >>> test5' == test5
-- True
-- >>> test5
-- fromList [(('B','B'...

simpleCounter :: String -> Map Char Int
simpleCounter string = fromListWith (+) $ fmap (,1) string

-- |
-- >>> simpleCounter "banana"
-- fromList [('a',3),('b',1),('n',2)]

countChars :: CountPair -> Char -> Map Char Int 
countChars countPairs lastChar   = insertWith (+) lastChar 1 leftAmounts 
    where  leftAmounts  = mapKeysWith (+) fst countPairs


-- |
-- >>> countChars test2 'B' == simpleCounter "banana"
-- False
-- >>> countChars test2 'B' == simpleCounter "NCNBCHB"
-- True


test30 = countChars (expandPairsManyTimes rules (countString start) 10) (last start)
    where (start,rules) = getInput testRules

-- |
-- >>> test30
-- fromList [('B',1749),('C',298),('H',161),('N',865)]

ans :: Map Char Int -> Int 
ans m = (maximum amounts) - (minimum amounts)
    where amounts = fmap snd $ toList m


goFor :: Int -> IO Int
goFor steps = do
    input <- readFile "2021/input14"
    let (start,rules) = getInput input
    return $ ans $ countChars (expandPairsManyTimes rules (countString start) steps) (last start)

-- |
-- >>> goFor 10
-- 2360
-- >>> goFor 40
-- 2967977072188


main = print 42