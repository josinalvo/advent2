{-# LANGUAGE TupleSections #-}

import Data.List ( nub, sort )
import Data.Map.Strict (Map, fromList, (!?), fromListWith, toList, insertWith, insert)
import Data.Char ( isLower )
import Data.Maybe (fromJust, isNothing, fromMaybe, isJust)
import Data.List.Extra (splitOn)
import Control.Monad (join, guard)
import Data.List.Utils (countElem)


import Debug.Trace (trace)
import Control.DeepSeq (deepseq)
import GHC.IO (unsafePerformIO)

type Noh = String

nodes :: [(Noh,Noh)] -> [Noh]
nodes pairs = nub (map fst pairs ++ map snd pairs)

adjs :: [(Noh,Noh)] -> Noh -> [Noh]
adjs pairs start =  firsts ++ secnds
    where firsts = map fst $ filter ((== start) . snd) pairs
          secnds = map snd $ filter ((== start) . fst) pairs

allAdjs :: [(Noh,Noh)] -> Map Noh [Noh]
allAdjs pairs = fromList [(noh,adjs pairs noh) | noh <- nodes pairs]

type Caminho = [Noh]
type Usados = Map Noh Int
data EstadoCaminho = EstadoCaminho {caminho::Caminho,usados :: Usados}


forbidden1 :: Usados -> Noh ->  Bool

forbidden1 caminho no = lower && used
    where lower = isLower (head no) 
          used  = no `elem` (map fst (toList caminho))

-- |
-- >>> forbidden1 (fromList [("A",1),("start",1)]) "A"
-- False
-- >>> forbidden1 (fromList [("a",1),("start",1)]) "a"
-- True


forbidden2 ::  Usados -> Noh -> Bool
forbidden2 mapa no = startEnd || lowerOtherFault || lowerMyFault
    where startEnd         = no `elem` ["start","end"] && (contagemNoh >= 1)
          maybeContNoh     = (!?) mapa no :: Maybe Int
          contagemNoh      = fromMaybe 0 maybeContNoh
          lowerOtherFault  = isLower (head no) && (contagemNoh == 1) && repeatedLower
          lowerMyFault     = isLower (head no) && (contagemNoh == 2)
          repeatedLower    = isJust (mapa !? "banana")

--forbidden a b | trace ("forbidden" ++ show a ++ " --- "++ show b ++ "r" ++ 
--                            show (forbidden1 a b) ++ show (map fst (toList b))) False = undefined

forbidden = forbidden2

-- versao com backtracking mais explicito
re2 :: Map Noh [Noh] -> Noh -> [Noh] -> EstadoCaminho -> [Caminho]
re2 adjs no []                       EstadoCaminho{caminho = caminho, usados=mapa} = [caminho | no == "end"]
re2 adjs no lista_vizinhos_pendentes EstadoCaminho{caminho = caminho, usados=mapa} = continuaCandidato ++ continuaDemais
    where (candidato:resto) = lista_vizinhos_pendentes
          continuaCandidato = if forbidden mapa candidato
                              then []
                              else re2 adjs candidato (fromJust (adjs !? candidato)) $ EstadoCaminho (candidato:caminho) hackedMap
          newMap            = (insertWith (+) candidato 1 mapa)
          hackedMap         = if (fromJust (newMap !? candidato) == 2 && isLower (head candidato))
                              then insert "banana" 100 newMap
                              else newMap
          continuaDemais    = re2 adjs no resto $ EstadoCaminho caminho mapa

re3 :: Map Noh [Noh] -> Noh -> EstadoCaminho -> [Caminho]
re3 adjs no  EstadoCaminho{caminho = caminho, usados=mapa} = atual ++ novos
    where lista_vizinhos = (fromJust (adjs !? no))
          novos = do
              vizinho <- lista_vizinhos
              guard $ (not . forbidden mapa) vizinho
              let novoCaminho = vizinho:caminho
              let tempMapa    = insertWith (+) vizinho 1 mapa
              let novoMapa    = if (fromJust (tempMapa !? vizinho) == 2 && isLower (head vizinho))
                                then insert "banana" 100 tempMapa
                                else tempMapa
              re3 adjs vizinho  $ EstadoCaminho novoCaminho novoMapa
          atual = [caminho | head caminho == "end"]
              

re :: Map Noh [Noh] -> Noh -> p -> EstadoCaminho -> [Caminho]
re adjs no unused estadoCaminho = re3 adjs no estadoCaminho
--re = re2
                              
pairs :: String -> [(Noh, Noh)]
pairs string = map pair $ lines string
    where pair string = (a,b)
            where [a,b] = splitOn "-" string

work fileName = do
    input <- readFile fileName
    let ps          = pairs input
    let adjs        = allAdjs ps
    let secondSteps = fromJust (adjs !? "start")
    let mapaStart   = fromList [("start",1)] :: Map String Int
    print $ length $ sort $ map (join . reverse) $ re adjs "start" secondSteps $ EstadoCaminho ["start"] mapaStart

    

-- | tests part1



-- >>> work "2021/input12mini"
-- 1
-- >>> work "2021/input12small"
-- 10
-- >>> work "2021/input12medium"
-- 19
-- >>> work "2021/input12large"
-- 226
-- >>> work "2021/input12"
-- 3679

-- | tests part2
-- >>> work "2021/input12small"
-- 36
-- >>> work "2021/input12medium"
-- 103
-- >>> work "2021/input12large"
-- 3509
-- >>> work "2021/input12"
-- 107395

main = work "input12"

teste :: Show a => String -> a  -> b -> b
teste s a = deepseq  $ unsafePerformIO (print $ s ++ show a ++ " ,fuck yes")