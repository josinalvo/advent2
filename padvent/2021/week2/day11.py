
flashed = 100

def readFile(fileName):
    file = open(fileName,'r')
    resulting = {}
    for (line_idx,line_content) in enumerate(file):
        for (column_idx, position_content) in enumerate(line_content):
            if position_content != '\n':
                resulting[(line_idx,column_idx)] = int(position_content)
    return resulting

def increase(matrix):
    for key in matrix:
        matrix[key] += 1

def neighs(l,c):
    def sumTuple (t1,t2):
        (l,c) = t1
        (x,y) = t2
        return (l+x,c+y)
    tuples = [(a,b) for a in [0,1,-1] for b in [0,1,-1] if (a,b) != (0,0)]
    return [sumTuple ((l,c),(x,y)) for (x,y) in tuples]

assert(neighs(1,1) == [     (1,2),(1,0),  (2,1),(2,2),(2,0),  (0,1),(0,2),(0,0) ])

def ready_to_flash(n):
    return 10 <= n < flashed

def flood_r(l,c,matrix):
    for (a,b) in neighs(l,c):
        if matrix.get((a,b),"banana") != "banana":
            matrix[(a,b)] += 1
        if ready_to_flash(matrix.get((a,b),flashed)):
            matrix[(a,b)] = flashed
            flood_r(a,b,matrix)
        


def flood(matrix):
    for (l,c) in matrix:
        valPos = matrix.get((l,c),flashed+200)
        if  ready_to_flash(valPos):
            matrix[(l,c)] = flashed
            flood_r(l,c,matrix)

def clean_and_count(matrix):
    cont = 0
    for key in matrix:
        if matrix[key] >= flashed:
            matrix[key] = 0
            cont += 1
    return cont

def step(matrix):
    increase(matrix)
    flood(matrix)
    return clean_and_count(matrix)

def show(matrix):
    for lineN in range(0,13):
        lineS = ''
        if matrix.get((lineN,0),'banana') == 'banana':
            break
        for colN in range(0,13):
            if matrix.get((lineN,colN),'banana') == 'banana':
                break
            lineS += str(matrix[(lineN,colN)])
        print (lineS)

def allFlashed(matrix):
    return set(matrix.values()) == set([0])

def showL(matrix):
    for lineN in range(0,13):
        lineS = []
        if matrix.get((lineN,0),'banana') == 'banana':
            break
        for colN in range(0,13):
            if matrix.get((lineN,colN),'banana') == 'banana':
                break
            lineS.append(matrix[(lineN,colN)])
        print (lineS)


def main(fileName):
    steps = 0
    first_superflash = None
    matrix = readFile(fileName)
    for i in range(1,400):
        steps += step(matrix)
        if not first_superflash and allFlashed(matrix):
            first_superflash = i

    
    show(matrix)

    # increase(matrix)
    # increase(matrix)
    # flood(matrix)
    # showL(matrix)
    
    
    print("steps: ",steps)
    print("superflash", first_superflash)

main("input11")

