{- HLINT ignore "incomplete patterns" -}
{- HLINT ignore "Eta Reduce" -}

import Data.List (elemIndex)
import Data.Maybe (isNothing, fromJust)
import Debug.Trace (trace)
import Data.Map ( (!?), fromList, Map ) 
import Numeric.Statistics.Median (median)
import GHC.Float (double2Int)


type Stack = [Char]

push stack char = char:stack

pop :: Stack -> (Stack, Maybe Char)
pop (c:stack) = (stack, Just c)
pop []        = ([]   , Nothing)


type StepLog     = ((String,Stack),Error Char)
data Error a     = UnfittingClosed a | ExcessClose a | MissingClose | NoErrorCont | NoErrorEnd | InvalidSymbol a deriving (Eq,Show)
-- Ele consegue retornar tudo isso?

opens :: [Char]
opens  = "([{<"
closes :: [Char]
closes = ")]}>"


fits :: Maybe Char -> Char -> Bool
fits (Just qc) c 
    | qc `notElem` opens  = error "Unexpected char in queue"
    | c `notElem` closes  = error "Unexpected char in string"
    | posQC == posC       = True
    | otherwise           = False
    where posQC = elemIndex qc opens
          posC  = elemIndex  c closes
fits Nothing c = error "Unexpected input"   

-- |
-- >>> fits (Just '(') ')'
-- True
-- >>> fits (Just '[') ')'
-- False
-- >>> fits (Just '[') ']'
-- True



step :: (String,Stack) -> StepLog
-- step (a,b) | trace ("string " ++ a ++ " stack: " ++ show b ++ "\n") False = undefined
step (c:newString,stack)  
    | c `elem` opens = (,) (newString,push stack c) NoErrorCont
    | c `notElem` opens ++ closes  = error "invalid sybol in string"
    | c `elem` closes = popConsequence popped
        where (newStack,popped) = pop stack
              newState = (newString,newStack)
              popConsequence popped
                | isNothing popped = (newState, ExcessClose c)
                | fits popped c    = (newState, NoErrorCont)
                | otherwise        = (newState, UnfittingClosed c)
    
step ("",stack@(c:stackRest))  = (,) ("",stack) MissingClose
step ("",stack@[])             = (,) ("",stack) NoErrorEnd
step _ = error "this step was not meant to be taken"

manyStep :: (String,Stack) -> StepLog
manyStep (string,stack) = if error == NoErrorCont 
                               then manyStep (newString,newStack)
                               else ((,) newString newStack,error)
    where ((,) newString newStack ,error) = step (string,stack)


result :: String -> Error Char
result string = snd $ manyStep (string,[])

-- |
-- >>> result "()"
-- NoErrorEnd
-- >>> result "(()"
-- MissingClose
-- >>> result "(()>"
-- UnfittingClosed '>'
-- >>> result "()[](<[]{}>)"
-- NoErrorEnd
-- >>> result "()[](<[]{}>)[]"
-- NoErrorEnd
-- >>> result "()[](<[]{}>)[]"
-- NoErrorEnd

-- >>> result "()[](<[]{}>)[])"
-- ExcessClose ')'

valuesUnfit :: Map Char Int
valuesUnfit = fromList [(')',3),
              (']',57),
              ('}',1197),
              ('>',25137)]

accountForChar1 :: Error Char -> Int
accountForChar1 (UnfittingClosed a) = fromJust $ valuesUnfit !? a
accountForChar1 _                   = 0

accountForString1 :: String -> Int
accountForString1 string = accountForChar1 $ result string
main1 file = do
    tests <- lines <$> readFile file
    print $ sum (fmap accountForString1 tests)
    --print $ (fmap result tests)

-- |
-- >>> main1 "2021/input10small"
-- 26397
-- >>> main1 "2021/input10"
-- 392097


valuesComplete :: Map Char Int
valuesComplete = fromList [('(',1), ('[',2), ('{',3), ('<',4)]

accountIncomplete :: String -> Int
accountIncomplete string = foldl (\acc v -> acc*5 + v) 0 calculatedVals
    where ((_,lastStack),error) = manyStep (string,[])
          stackTopToBottom = lastStack
          vals MissingClose lastStack = map (fromJust . (valuesComplete !?)) stackTopToBottom
          vals _            _         = [0]
          calculatedVals :: [Int]
          calculatedVals              = vals error lastStack


-- |
-- >>> accountIncomplete "[({(<(())[]>[[{[]{<()<>>"
-- 288957
-- >>> accountIncomplete "(((({<>}<{<{<>}{[]{[]{}"
-- 1480781

accountIncompletes :: [String] -> Int
accountIncompletes strings = double2Int $ median $ filter (/= 0) amounts
    where amounts = map (fromIntegral . accountIncomplete) strings



part2 :: String -> IO Int
part2 file = accountIncompletes . lines <$> readFile file

-- |
-- >>> part2 "2021/input10small"
-- 288957
-- >>> part2 "2021/input10"
-- 4263222782


main = print 42