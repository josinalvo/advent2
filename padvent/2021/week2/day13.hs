import Data.List.Extra (splitOn, nub)
type Point = (Int,Int)

toFun :: String -> (Point -> Point)
toFun s p = change axis p
    where [_, _, var]         = splitOn " " s
          [axis,posString]    = splitOn "=" var
          pos                 = read posString
          mirror1d line point = if point > line
                                then line-(point-line)
                                else point
          change :: String -> Point -> Point
          change "x" (a,b)       = (,) (mirror1d pos a) b
          change "y" (a,b)       = (,) a (mirror1d pos b) 

a = toFun "fold along x=40"

-- |
-- >>> a (41,400)
-- (39,400)
-- >>> a (70,500)
-- (10,500)


readPoint :: [ Point->Point ] -> String -> Point
readPoint fs tupleS = afterPoint
    where [x,y]      = map read $ splitOn "," tupleS
          afterPoint :: Point
          afterPoint = foldr ($) (x,y) fs

-- |
-- >>> readPoint [a,a,a] "41,400"
-- (39,400)
          
points :: String -> [Point->Point] -> [Point]
points s fs = map (readPoint fs) (lines relevantText)
    where relevantText = splitOn "\n\n" s !! 0
          



readPoints fs = do
    input <- readFile "2021/input13"
    let onlyPoints = points input fs
    return onlyPoints

-- | readPoints []
-- >>> readPoints []
-- [(938,670)...(572,718)]

f = toFun "fold along x=655"


-- | 
-- >>> length <$> nub <$> readPoints [f]
-- 745

fs :: String -> [Point -> Point]
fs s = reverse . map toFun $ lines relevantText
    where relevantText = splitOn "\n\n" s !! 1


read2 = do
    input <- readFile "2021/input13"
    let paperFolds = fs input
    let onlyPoints = points input paperFolds
    return onlyPoints

range :: [Point] -> (Point,Point)
range points = (,) (minimum xs, minimum ys) (maximum xs, maximum ys)
    where xs = map fst points
          ys = map snd points

-- |
-- >>> range <$> read2
-- ((0,0),(38,5))

showDisplay :: [Point] -> String
showDisplay ps = concat $ map line ys
    where  ((minX,minY),(maxX, maxY)) = range ps
           xs = [minX..maxX]
           ys = [minY..maxY]
           line :: Int -> String
           line y = (++ "\n") $ map (showChar y) xs
           showChar y x = if (x,y) `elem` ps then 'o' else ' '


read3 = do
    input <- readFile "2021/input13"
    let paperFolds = fs input
    let onlyPoints = points input paperFolds
    putStr $ showDisplay onlyPoints

-- |
-- >>> read3
--  oo  ooo  o  o   oo oooo ooo   oo   oo 
-- o  o o  o o o     o o    o  o o  o o  o
-- o  o ooo  oo      o ooo  ooo  o    o   
-- oooo o  o o o     o o    o  o o oo o   
-- o  o o  o o o  o  o o    o  o o  o o  o
-- o  o ooo  o  o  oo  o    ooo   ooo  oo 
main = read3