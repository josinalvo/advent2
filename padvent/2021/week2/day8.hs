
import Data.List.Split
import Data.List (permutations, sort, elemIndex)
import Data.Set as Set ( fromList )
import Data.Maybe

-- nao super conferi, mas passou e nao pretendo usar pra parte 2
countUnique :: String -> Int
countUnique string = length $ Prelude.filter  ((`elem` [2,4,3,7]) . length) relevantPart
    where relevantPart = (splitOn " ") . (!! 1) . (splitOn " | ") $ string

-- |
-- >>> a = "fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg"
-- >>> countUnique a
-- 3


main1 = do
    s <- lines <$> readFile "input8"
    let counts = fmap countUnique s
    return $ sum counts

data Segment = High | Low | Middle | HighLeft | HighRight | LowLeft | LowRight deriving (Eq, Show, Ord)
digits = [[High,Low,HighLeft,HighRight,LowLeft,LowRight],
          [HighRight,LowRight],
          [High,HighRight,Middle,LowLeft,Low],
          [High,HighRight,Middle,LowRight,Low],
          [HighLeft,HighRight,Middle,LowRight],
          [High,HighLeft,Middle,LowRight,Low],
          [High,HighLeft,Middle,LowRight,LowLeft,Low],
          [High,HighRight,LowRight],
          [High, HighRight, HighLeft, Middle, LowRight, LowLeft, Low],
          [High, HighRight, HighLeft, Middle, LowRight, Low]
          ]

showDigit :: [Segment] -> String 
showDigit segments = [s , h, h, s , n,
                      hl, s, s, hr, n,
                      hl, s, s, hr, n,
                      s , m , m, s, n,
                      ll, s, s, lr, n,
                      ll, s, s, lr, n,
                      s , l, l, s , n
                      ]
    where s = ' '
          h = if High `elem` segments then '-' else ' '
          m = if Middle `elem` segments then '-' else ' '
          l = if Low `elem` segments then '-' else ' '
          hl = if HighLeft `elem` segments then '|' else ' '
          hr = if HighRight `elem` segments then '|' else ' '
          lr = if LowRight `elem` segments then '|' else ' '
          ll = if LowLeft `elem` segments then '|' else ' '
          n = '\n'

showAllDigits = mapM putStr $ fmap showDigit digits

type Key = [Segment]

allSegments :: [Segment]
allSegments = digits !! 8

allPossiblePositions :: [Key]
allPossiblePositions = permutations allSegments

segmentsToString :: Key -> [Segment] -> String
segmentsToString key segments = segmentChars
    where segmentNums  = fmap (fromJust . (`elemIndex` key)) segments
          segmentChars = sort $ fmap ("abcdefg" !!) segmentNums

match ::  String -> Key -> Bool
match allEncodedStrings key = Set.fromList digitStrings == Set.fromList positionStrings
    where digitStrings = fmap sort $ splitOn " " allEncodedStrings
          positionStrings = fmap (segmentsToString key) digits

getValidPosition :: String -> Key
getValidPosition allEncodedStrings = head $ filter (match allEncodedStrings) allPossiblePositions 

--  dddd
-- e    a
-- e    a
--  ffff
-- g    b
-- g    b
--  cccc

-- | 
-- >>> getValidPosition "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab"
-- [HighRight,LowRight,Low,High,HighLeft,Middle,LowLeft]

translate :: Key -> String -> Int
translate validKey string = number
    where charToPos  :: Char -> Int
          charToPos        char = fromJust $ elemIndex char "abcdefg"
          charToSeg  :: Char -> Segment
          charToSeg        char = validKey !! charToPos char
          niceDigits :: [[Segment]]
          niceDigits            = fmap sort digits
          charList2segs :: String -> [Segment]
          charList2segs   chars = sort $ fmap charToSeg chars
          charList2digit :: String -> Int
          charList2digit  chars = fromJust $ elemIndex (charList2segs chars) niceDigits
          listDigits :: [Int]
          listDigits            = fmap charList2digit $ splitOn " " string
          number :: Int
          number                = read $ concat $ fmap show listDigits

-- | 
-- >>> p = getValidPosition "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab"
-- >>> p
-- [HighRight,LowRight,Low,High,HighLeft,Middle,LowLeft]
-- >>> translate p "cdfeb fcadb cdfeb cdbaf"
-- 5353


decodeTranslate :: String -> Int
decodeTranslate string = number
    where observations :: String
          observations = (splitOn " | " string) !! 0
          obfuscatedNumber :: String
          obfuscatedNumber = (splitOn " | " string) !! 1
          -- [observations, obsfucatedNumber] = splitOn " | " string
          -- gostaria de escrever assim e declarar os tipos
          --[observations, obsfucatedNumber] = splitOn " | " string
          position = getValidPosition observations
          number :: Int
          number   = translate position obfuscatedNumber

-- |
-- >>> decodeTranslate "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe"
-- 8394



main2 = do
    s <- lines <$> readFile "input8"
    let values = fmap decodeTranslate s
    print $ sum values

main = main2