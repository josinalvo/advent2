import Data.List.Extra
import Control.Monad

import Data.Maybe
import Debug.Trace

import Data.Vector (fromList, Vector, (!?))

type Matrix = (Int,Vector Int)
data Position = Position { line :: Int, column :: Int} deriving (Show,Eq)

readPos :: Matrix -> Position  -> Maybe Int
readPos (lineSize, vals) p  = do
                               guard (column p < lineSize && column p >= 0)
                               vals !? (lineSize * (line p) + column p)

mFromString :: String -> Matrix
mFromString string = (size,fromList list)
        where list     = map (read . char2str) $ filter (/= '\n') string
              size     = length $ head $ lines string
              char2str = (:[])
            
small :: String
small = "2199943210\n3987894921\n9856789892\n8767896789\n9899965678\n"

mSmall :: Matrix
mSmall = mFromString small
-- |
-- >>> readPos mSmall (Position 1 1) 
-- Just 9
-- >>> readPos mSmall (Position 2 9)
-- Just 2
-- >>> readPos mSmall (Position 2 10)
-- Nothing
-- >>> readPos mSmall (Position 4 9)
-- Just 8
-- >>> readPos mSmall (Position 5 9)
-- Nothing
-- >>> readPos mSmall (Position 5 1)
-- Nothing
-- >>> readPos mSmall (Position 5 0)
-- Nothing

lineSize matrix = fst matrix
columnSize matrix = length (snd matrix) `div` (lineSize matrix)

-- |
-- >>> lineSize mSmall
-- 10
-- >>> columnSize mSmall
-- 5


neighboorsAsPos ::  Matrix -> Position -> [Position]
neighboorsAsPos matrix p = map (uncurry Position) clean
    where (l,c)          = (line p, column p)
          possibilities  = [(l+1,c),(l,c+1),(l-1,c),(l,c-1)]
          valid (a,b)    = isJust $ readPos matrix (Position a b)
          clean          = filter valid possibilities

-- |
-- >>> neighboorsAsPos mSmall (Position 0 1)
-- [Position {line = 1, column = 1},Position {line = 0, column = 2},Position {line = 0, column = 0}]
-- >>> neighboorsAsPos mSmall (Position 0 0)
-- [Position {line = 1, column = 0},Position {line = 0, column = 1}]
-- >>> neighboorsAsPos mSmall (Position 4 9)
-- [Position {line = 3, column = 9},Position {line = 4, column = 8}]

neighboorsByVal ::  Matrix -> Position -> [Int]
neighboorsByVal matrix p = fmap (fromJust . readPos matrix) positions
    where positions = neighboorsAsPos matrix p



-- |
-- >>> neighboorsByVal mSmall (Position 0 1)
-- [9,9,2]
-- >>> neighboorsByVal mSmall (Position 4 9)
-- [9,7]



isMinimum :: Matrix -> Position  -> Bool
isMinimum matrix p = all (> center) l_neibs
    where l_neibs    = neighboorsByVal matrix p
          center     = fromJust $ readPos matrix p


-- |
-- >>> isMinimum mSmall (Position 0 1) 
-- True
-- >>> isMinimum mSmall (Position 1 1)
-- False
-- >>> isMinimum mSmall (Position 4 6)
-- True

allPos :: Matrix -> [Position]
allPos matrix = [Position line col | line <- [0..max_line], col  <- [0..max_col]]
    where max_col  = (lineSize   matrix) -1
          max_line = (columnSize matrix) -1

-- | 
-- >>> allPos mSmall
-- [Position {line = 0, column = 0},Position {line = 0, column = 1},Position {line = 0, column = 2},...Position {line = 0, column = 9},Position {line = 1, column = 0},Position {line = 1, column = 1},Position {line = 1, column = 2},Position {line = 1, column = 3},Position {line = 1, column = 4},Position {...


allMins :: Matrix -> [Position]
allMins matrix = filter (isMinimum matrix) (allPos matrix)

-- |
-- >>> allMins mSmall
-- [Position {line = 0, column = 1},Position {line = 0, column = 9},Position {line = 2, column = 2},Position {line = 4, column = 6}]

cost_part1 :: Matrix -> Int 
cost_part1 matrix = sum costs
    where min_positions = allMins matrix
          costs :: [Int]
          costs    = map  ((+1) . fromJust . readPos matrix) min_positions

-- |
-- >>> cost_part1 mSmall
-- 15

main1 file = readFile file >>= (print . cost_part1 . mFromString)
    


-- |
-- >>> main1 "2021/input9small"
-- 15
-- >>> main1 "2021/input9"
-- 508


data Exploration = Exploration {inners::[Position], parents::[Position], currents::[Position]} deriving Show

explore :: Matrix ->  Exploration -> Exploration
--explore a b | trace ("explore " ++ show (currents b) ++ "\n") False = undefined
explore matrix (Exploration inners parents currents)  = Exploration {inners = (currents++inners), parents = currents, currents = notHigh} 
    where newCandidates = currents >>= neighboorsAsPos matrix
          newSelected   = filter (not . (`elem` parents)) newCandidates
          notHigh       = nub $ filter ((/= Just 9) . readPos matrix) newSelected

-- |
-- >>> a = explore (mFromString small) (Exploration [] [] [Position 0 1])
-- >>> print a
-- Exploration {inners = [Position {line = 0, column = 1}], parents = [Position {line = 0, column = 1}], currents = [Position {line = 0, column = 0}]}
-- >>> print $ explore (mFromString small)  a
-- Exploration {inners = [Position {line = 0, column = 0},Position {line = 0, column = 1}], parents = [Position {line = 0, column = 0}], currents = [Position {line = 1, column = 0}]}

exploreTill :: Matrix -> Exploration ->  Exploration
exploreTill matrix e@(Exploration inners parents [])  =  e
exploreTill matrix e                                  =  exploreTill matrix afterOneStep
    where afterOneStep = explore matrix e


basins :: Matrix -> [Exploration]
basins matrix = basinsList
    where emptyExp   = Exploration {inners = [], parents = [], currents = []}
          initialExps= fmap (\x -> emptyExp { currents = [x] }) (allMins matrix)
          basinsList = fmap (exploreTill matrix) initialExps

basinsCost matrix  = product $ take 3 $ reverse $ sort $ fmap (length . inners) (basins matrix)


-- |
-- >>> basinsCost mSmall
-- 1134

main2 file = (basinsCost <$> mFromString <$> readFile file ) >>= print 

-- |
-- >>> main2 "2021/input9small"
-- 1134

main = main2 "2021/input9"

-- |
-- >>> main
-- 1564640