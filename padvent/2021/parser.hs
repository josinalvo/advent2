-- http://book.realworldhaskell.org/read/using-parsec.html
-- http://jakewheat.github.io/intro_to_parsing/#getting-started
-- import Text.ParserCombinators.Parsec
import Text.Parsec
import Data.Functor.Identity

separator :: ParsecT String u Data.Functor.Identity.Identity [Char]
separator = newLines >> string "--- scanner " >> (many digit) >>  string " ---" >> newLines

newLines :: ParsecT String u Identity [Char]
newLines = (many $ char '\n')

scanners :: ParsecT String u Identity [[[Int]]]
scanners = separator >> sepBy beacons separator <* newLines
beacons :: ParsecT String u Identity [[Int]]
beacons = endBy1 beacon (char '\n')
beacon :: ParsecT String u Identity [Int]
beacon = fmap toInt <$> sepBy1 coord (char ',')


toInt :: String -> Int
toInt = read

coord :: ParsecT String u Identity [Char]
coord =  ((:) <$> char '-' <*> many1 digit) <|> many1 digit

type A = Int

parseCSV :: String -> Either ParseError [[[A]]]
parseCSV input = parse scanners "(unknown)" input

main = do
    file <- readFile "input19small"
    print $ parseCSV file
