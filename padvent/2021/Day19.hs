module Day19 where

import Data.Set hiding (map)

pos = [0,1,2]
permutacoes = [[a,b,c] | a <- pos, b <- pos, c <- pos, a /= b, a /= c, b /= c]

sign = [-1,1]
sinais = [[a,b,c] |a <- sign, b <- sign, c <- sign]

type Coords = [Int]
type Perm   = [Int]
type Sign   = [Int]

apply :: Coords -> Perm -> Sign -> Coords
apply coords perm sign = zipWith (*) vals sign
    where vals = map (coords !!) perm

-- |
-- >>> apply [10,20,30] [0,2,1] [-1,-1,1]
-- [-10,-30,20]

translate :: Coords -> Coords -> [Coords] -> [Coords]
translate ref member members = map (zipWith (+) diff) members
    where diff = zipWith (-) ref member
          
-- |
-- >>> translate [0,0] [10,20] [[10,20],[11,21],[9,23]]
-- [[0,0],[1,1],[-1,3]]

similarity :: [Coords] -> [Coords] -> Perm -> Sign -> Int
similarity coord1 coord2 perm sign = maximum similarities
    where newCoord2 = map (\x -> apply x perm sign) coord2
          similarities :: [Int]
          similarities = do
              c1 <- coord1
              c2 <- newCoord2
              let translated2 = translate c1 c2 newCoord2
              let set1 = fromList coord1 :: Set [Int]
              let set2 = fromList translated2 :: Set [Int]
              let united = intersection set1 set2
              return $ size united

scan0 = [[404,-588,-901],[528,-643,409],[-838,591,734],[390,-675,-793],[-537,-823,-458],[-485,-357,347],[-345,-311,381],[-661,-816,-575],[-876,649,763],[-618,-824,-621],[553,345,-567],[474,580,667],[-447,-329,318],[-584,868,-557],[544,-627,-890],[564,392,-477],[455,729,728],[-892,524,684],[-689,845,-530],[423,-701,434],[7,-33,-71],[630,319,-379],[443,580,662],[-789,900,-551],[459,-707,401]]

scan1 = [[686,422,578],[605,423,415],[515,917,-361],[-336,658,858],[95,138,22],[-476,619,847],[-340,-569,-846],[567,-361,727],[-460,603,-452],[669,-402,600],[729,430,532],[-500,-761,534],[-322,571,750],[-466,-666,-811],[-429,-592,574],[-355,545,-477],[703,-491,-529],[-328,-685,520],[413,935,-424],[-391,539,-444],[586,-435,557],[-364,-763,-893],[807,-499,-711],[755,-354,-619],[553,889,-390]]

a = do
    perm <- permutacoes
    sign <- sinais
    return $ similarity scan0 scan1 perm sign

-- |
-- >>> a
-- [...12...]

main = print 42